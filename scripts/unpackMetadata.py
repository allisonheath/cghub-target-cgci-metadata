#!/usr/bin/env python

import argparse
import common
import csv
import re
import subprocess
import sys

parser = argparse.ArgumentParser()
parser.add_argument('runTableFile', type=str, help="manifest file")
parser.add_argument('accessionsFile', type=str, help="SRA Accessions file")
parser.add_argument('metadataTarFile', type=str,
                    help="SRA Metadata dump")
parser.add_argument('metadataOutputDir', type=str, help="Output directory")
parser.add_argument('center', type=str, help="GSC")
args = parser.parse_args()

csv.field_size_limit(sys.maxsize)
accessions = common.readDictOfDicts(args.accessionsFile)
runTable = csv.DictReader(open(args.runTableFile), delimiter="\t")
submissionsUnpacked = dict()
for row in runTable:
    cleanedRow = common.removeExtraneousWhitespaceFromDict(row)
    runAccession = cleanedRow['Run']
    if accessions[runAccession]['Type'] != 'RUN':
        raise ValueError("Unexpected accession type %s from accession %s"
                         % (accessions[runAccession]['Type'], runAccession))
    center = cleanedRow[runAccession]['Center Name']
    if center == args.center:
        submissionAccession = accessions[runAccession]['Submission']
        if not submissionsUnpacked.has_key(submissionAccession):
            print "Getting %s from run accession %s" % (submissionAccession,
                                                        runAccession)
            untarCmd = "tar xzvf %s %s/%s" % (args.metadataTarFile,
                                              args.metadataOutputDir,
                                              submissionAccession)
            subprocess.check_output(untarCmd, shell=True)
            submissionsUnpacked[submissionAccession] = 1
