#!/usr/bin/env python

import argparse
import common
import os
import re
import sys
import xml.etree.ElementTree as ET

def unpackObjects(pathname, perObjectDir, label):
    """Unpack each object from the SRA metadata, which are
    packaged in set objects.  Make a separate file for
    each object, named by accession"""
    print "label", label
    tree = ET.parse(pathname)
    root = tree.getroot()
    for child in root:
        if not child.attrib.has_key(label):
            print label, child.attrib.keys(), (label in child.attrib.keys())
        assert(child.attrib.has_key(label))
        thisLabel = child.attrib.get(label)
        thisLabel = re.sub("/", "", thisLabel)
        print "working on accession", thisLabel
        outputFile = perObjectDir + "/" + thisLabel + ".xml"
        if os.path.exists(outputFile):
            print "File already exists", outputFile, "from label", thisLabel, label
        else:
            ET.ElementTree(child).write(outputFile)



parser = argparse.ArgumentParser()
parser.add_argument('sraDumpDir', type=str,
                    help="Input directory: pathname of the SRA metadata dump")
parser.add_argument("perObjectDir", type=str,
                    help="Output directory for the repacked metadata")
parser.add_argument("--label", type=str, default="accession",
                    help="Label to use as the filename for the extracted objs")
args = parser.parse_args()

if not os.path.exists(args.perObjectDir):
    os.mkdir(args.perObjectDir)
    
experimentTargetDir = args.perObjectDir + "/experiment"
if not os.path.exists(experimentTargetDir):
    os.mkdir(experimentTargetDir)
runTargetDir = args.perObjectDir + "/run"
if not os.path.exists(runTargetDir):
    os.mkdir(runTargetDir)
analysisTargetDir = args.perObjectDir + "/analysis"
if not os.path.exists(analysisTargetDir):
    os.mkdir(analysisTargetDir)

for root, dirs, files in os.walk(args.sraDumpDir):
    print "Looking at", files
    for thisFile in files:
        if re.search("experiment.*.xml$", thisFile):
            print "unpacking", thisFile
            unpackObjects(root + "/" + thisFile, experimentTargetDir, 
                          args.label)
        if re.search("run.*.xml$", thisFile):
            print "unpacking", thisFile
            unpackObjects(root + "/" + thisFile, runTargetDir, args.label)
        if re.search("analysis.*.xml$", thisFile):
            print "unpacking", thisFile
            unpackObjects(root + "/" + thisFile, analysisTargetDir, args.label)
        if re.search("new.analysis.xml$", thisFile):
            print "unpacking", thisFile
            unpackObjects(root + "/" + thisFile, analysisTargetDir, args.label)


