#!/usr/bin/env python

import argparse, re, os, sys, csv, string, errno
from collections import defaultdict

def cmdParse():
    "parse command line and return arguments object"
    parser = argparse.ArgumentParser(description="""Generate summary reports and details from the results of psychoanalyzeTarget""")
    parser.add_argument("reportTsv", type=str,
                        help="""report from psychoanalyzeTarget""")
    parser.add_argument("outdir", type=str,
                        help="""output directory""")
    parser.add_argument("seqtypeMap", type=str,
                        help="""file listing the srx2seqtype mapping""")
    parser.add_argument("barcodeStudyMap", type=str,
                        help="""file listing the srx2barcodes and srx2studies mapping""")
    return parser.parse_args()

def load_map(file):
   map_ = {}
   with open(file) as fh:
       for line in fh:
           if len(line) < 1:
               continue
           line = line.rstrip()
           fields = line.split("\t")
           if len(fields) == 1:
               map_[fields[0]] = 1
           elif len(fields) == 2:
               map_[fields[0]] = fields[1]
           else:
               map_[fields[0]]=fields
   return map_ 

args = cmdParse()
seqtypeMap = load_map(args.seqtypeMap)
barcodeStudyMap = load_map(args.barcodeStudyMap)

class excel_tab_unix(csv.excel_tab):
    lineterminator = '\n'

def ensureDir(dir):
    """Ensure that a directory exists, creating it (and parents) if needed."""
    try: 
        os.makedirs(dir)
    except OSError, e:
        if e.errno != errno.EEXIST:
            raise e

class ReportRow(object):
    "row from report TSV, file columns converted to fields"
    def __init__(self, tsvRow):
        self.__dict__.update(tsvRow)  # convert to fields
        self.tsvRow = tsvRow

    def __str__(self):
        fields = []
        for n in self.__dict__.iterkeys():
            if n[0] != '_': # is the is data field?
                fields.append(n+"="+str(getattr(self, n))) 
        return ", ".join(fields)

class PsychoReport(list):
    "holds results from psychoanalyzeTarget"
    def __init__(self, reportTsvFh, barcodeStudyMap):
        self.centers = set()
        tsv = csv.DictReader(reportTsvFh, dialect=csv.excel_tab)
        self.fieldnames = tsv.fieldnames
        self.fieldnames.append("tumorCode")
        for tsvRow in tsv:
            tsvRow["tumorCode"] = ""
            #something of a hack, if CGI and doesn't have a filetype (most likely because the source files have been removed, we assume a BAM)
            if tsvRow["center"] == "CompleteGenomics" and (tsvRow["filetype"] == "filetype" or tsvRow["filetype"] == "" or tsvRow["filetype"] == "CGI_BAM"):
                tsvRow["filetype"] = "BAM"
            if tsvRow["exprAcc"] in barcodeStudyMap and len(barcodeStudyMap[tsvRow["exprAcc"]]) > 3:
                tsvRow["tumorCode"] = barcodeStudyMap[tsvRow["exprAcc"]][3] 
            if tsvRow["sampleBarcode"] == "" and tsvRow["exprAcc"] in barcodeStudyMap and len(barcodeStudyMap[tsvRow["exprAcc"]]) > 1:
                tsvRow["sampleBarcode"] = barcodeStudyMap[tsvRow["exprAcc"]][1] 
            if tsvRow["study"] == "" and tsvRow["exprAcc"] in barcodeStudyMap and len(barcodeStudyMap[tsvRow["exprAcc"]]) > 2:
                tsvRow["study"] = barcodeStudyMap[tsvRow["exprAcc"]][2] 
            if tsvRow["center"] == "":
                tsvRow["center"]="EMPTY"
            self.centers.add(tsvRow["center"])
            self.__loadRow(tsvRow)

    def __loadRow(self, tsvRow):
        self.append(ReportRow(tsvRow))

class ReportGenerator(object):
    def __init__(self, psychoReport, columnNames, classifyFunction, filterFunction=None, sortKeyFunc=None):
        """ 
        columnNames - names of report columns
        classifyFunction - takes a ReportRow object and returns a tuple of report columns or None to ignore
        filterFunction - if specified, returns True if row should be included
        sortKeyFunc - if specified, get sort keys for classifyFunction tuple
        """
        self.psychoReport = psychoReport
        self.columnNames = columnNames
        self.classifyFunction = classifyFunction
        self.filterFunction = filterFunction
        self.sortKeyFunc = sortKeyFunc if sortKeyFunc != None else lambda r: r
        self.totalCount = 0
        self.categoryCounts = defaultdict(int)
        self.categorySamples = defaultdict(set)  # use to find out how many samples are in a category
        self.categoryRows = defaultdict(list)
        self.__classify()

    def __classifyRow(self, reportRow):
        category = self.classifyFunction(reportRow)
        if category != None:
            self.totalCount += 1
            self.categoryCounts[category] += 1
            self.categoryRows[category].append(reportRow)
            if reportRow.sampleAcc != None:
                self.categorySamples[category].add(reportRow.sampleAcc)

    def __classify(self):
        for reportRow in self.psychoReport:
            if (self.filterFunction == None) or self.filterFunction(reportRow):
                self.__classifyRow(reportRow)

    def __calcFreq(self, count):
        if self.totalCount == 0:
            return "NaN"
        else:
            return format(float(count)/float(self.totalCount), "0.3f")

    def __writeCategory(self, category, outTsv):
        count = self.categoryCounts[category]
        #outTsv.writerow(category + (count, self.__calcFreq(count), len(self.categorySamples.get(category, []))))
        outTsv.writerow(category + (count, self.__calcFreq(count)))

    def __writeTotal(self, outTsv):
        allSamples = set()
        for samples in self.categorySamples.itervalues():
            allSamples |= samples
        #outTsv.writerow(["Total"] + ["" for c in xrange(len(self.columnNames)-1)] + [self.totalCount, self.__calcFreq(self.totalCount), len(allSamples)])
        outTsv.writerow(["Total"] + ["" for c in xrange(len(self.columnNames)-1)] + [self.totalCount, self.__calcFreq(self.totalCount)])

    def __writeSummary(self, outTsvFile, includeFiletype):
        with open(outTsvFile, "w") as outTsvFh:
            outTsv = csv.writer(outTsvFh, dialect=excel_tab_unix)
            if includeFiletype:
                outTsv.writerow(self.columnNames + ("filetype", "count", "frequency"), )
            else:
                outTsv.writerow(self.columnNames + ("count", "frequency"), )
            for category in sorted(self.categoryCounts.iterkeys(), key=self.sortKeyFunc):
                self.__writeCategory(category, outTsv)
            self.__writeTotal(outTsv)

    __cleanTrans = string.maketrans(" -", "__")
    __deleteTrans = "()"
    def __cleanCategoryColumn(self, categoryColumn):
        if categoryColumn == "":
            return "None"
        else:
            return categoryColumn.translate(self.__cleanTrans, self.__deleteTrans)
        
    def __categoryFileName(self, category):
        return "-".join([self.__cleanCategoryColumn(cc) for cc in category])

    def __writeDetailFile(self, outTsvFile, category):
        with open(outTsvFile, "w") as outTsvFh:
            outTsv = csv.DictWriter(outTsvFh, self.psychoReport.fieldnames, dialect=excel_tab_unix)
            outTsv.writeheader()
            for reportRow in self.categoryRows[category]:
                outTsv.writerow(reportRow.tsvRow)

    def __writeDetails(self, detailsOutDir):
        ensureDir(detailsOutDir)
        for category in self.categoryCounts.iterkeys():
            self.__writeDetailFile(detailsOutDir + "/" + self.__categoryFileName(category) + ".tsv", category)
        
    def writeReport(self, outdir, baseName, includeFiletype=False):
        ensureDir(outdir)
        self.__writeSummary(outdir + "/" + baseName + ".report.tsv",includeFiletype)
        detailsOutDir = outdir + "/" + baseName + ".details"
        self.__writeDetails(detailsOutDir)

def isCghubDownloadOk(row):
    """is the a SRA submission downloaded that is destine for CGHub (guessed from files include),
    or if there is nothing, so we know it's ok
    """
    return row.downloadedDesc in ("absent", "chromSplitBam", "partialChromSplitBam", "singleBam", "released", "can_be_released")

def inclInStateReport(row):
    "include in reports"
    ##NO LONGER TRUE: must be at SRA or live at cghub and not a SRF download
    # some can be live at CGHub and SRA state unknown due to lack of run accession
    return (("live" in row.sraStatus) or ("live" in row.cghubDesc)) #and isCghubDownloadOk(row)

def getBaseStudy(study):
    if len(study) == 0:
        # assume TARGET if no study, this is because we don't have a run to map to it.
        return "TARGET"
    else:
        return study.split()[0]

#study_disesase_pattern = re.pattern.compile("([^\s+]+)\s+\(([^\)]+)\)")
study_disease_pattern = re.compile("^\(([^\)]+)\)$")
barcode_disease_pattern = re.compile("^TARGET-(\w\w)-")
def getBaseDisease(study,barcode_disease):
    if not study or len(study) == 0:
        # assume TARGET if no study, this is because we don't have a run to map to it.
        return "unknown:%s" % (barcode_disease) 
    else:
        splits = re.split("\s+",study)
        if len(splits) < 2:
            return "unknown:%s" % (barcode_disease)
        disease = " ".join(splits[1:])
        m = study_disease_pattern.search(disease)
        if m is None:
            return "unknown:%s" % (barcode_disease) 
        disease = m.group(1)
        return "%s:%s" % (disease,barcode_disease)

#only support TARGET barcodes
#barcode_pattern = re.compile("TARGET-\w\w-[^-]+-\d\d\w-\d\d(\w)")
barcode_pattern = re.compile("TARGET-.+([a-zA-Z])$")
def_seq_type_map = {"BCCAGSC:D":"WGS","BCCAGSC:R":"RNA","BI:W":"WXS","BI:D":"WXS","BCM:D":"WXS","CompleteGenomics:D":"WGS","CompleteGenomics:W":"WGS"}
def getBaseSeqType(srx,center,sampleBarcode):
    if srx in seqtypeMap:
        return seqtypeMap[srx]
    #fallback time
    if sampleBarcode is None or len(sampleBarcode) == 0 or center is None or len(center) == 0:
        return "unknown"
    m = barcode_pattern.search(sampleBarcode)
    if m is None:
        return "unknown"
    type = m.group(1)
    type = "%s:%s" % (center,type)
    if type not in def_seq_type_map:
        return type
    return def_seq_type_map[type]
   
#sizes are filesize in GBs 
seq_size_map = {"BCCAGSC:D":69,"BCCAGSC:R":9,"BI:D":14,"BCM:D":23,"CompleteGenomics:D":258}
def getBaseAvgSize(center,seqType):
    if seqType is None or len(seqType) == 0 or center is None or len(center) == 0:
        return 0
    key = "%s:%s" % (center,seqType)
    if key not in seq_size_map:
        return 0
    return seq_size_map[key]
            
#filetypes={'srf':['SRF',re.compile(r'\.srf'), 'gz':['FASTQ',re.compile(r'\.gz')], 'fastq':['FASTQ',re.compile(r'\.fastq')], 'bam':['BAM',re.compile(r'\.bam')], '':''}
filetypes={'srf':'SRF', 'gz':'FASTQ', 'fastq':'FASTQ', 'BAM':'BAM', 'bam':'BAM', 'CGI_BAM':'BAM'}
def getFileType(row):
    found = set()
    if len(row.filenames) > 0:
        files = row.filenames.split(",")
        for file in files:
            fparts = file.split(".")
            if len(fparts) > 1 and fparts[-1] in filetypes and filetypes[fparts[-1]] not in found:
                found.add(filetypes[fparts[-1]])
    #in case there are a mixture of file types on this line
    if len(found) > 0:
        return "/".join(sorted(found))
    return ""
         
        
def getDataState(row):
    if "notLoaded" not in row.cghubDesc:
        if "error" in row.cghubState:
            return "error"
        return row.cghubDesc
    elif "absent" in row.downloadedDesc:
        return "absent"
    elif "ok" in row.downloadedState:
        return "ready"
    else:
        return "error"

def getMetadataState(row):
    st = row.metadataDesc
    if "OK" in st:
        return "ready"
    elif "noMetadata" in st:
        return "unknown"
        #return "absent"
    elif "Failed" in st:
        return "error"
    else:
        return row.metadataDesc

def reportStateCenter(psychoReport, outdir):
    "report state including center, ignoring not live at SRA"
    rg = ReportGenerator(psychoReport, ("study", "center", "metadataState", "dataState"),
                         lambda row: (getBaseStudy(row.study), row.center, getMetadataState(row), getDataState(row)),
                         inclInStateReport)
    rg.writeReport(outdir, "center-state")

def reportStateSingleCenter(psychoReport, center, outdir):
    "report state including center, ignoring not live at SRA"
    rg = ReportGenerator(psychoReport, ("study", "center", "metadataState", "dataState"),
                         lambda row: (getBaseStudy(row.study), row.center, getMetadataState(row), getDataState(row)),
                         lambda row: (row.center==center) and inclInStateReport(row))
    rg.writeReport(outdir, center+"-state")


def reportStateSingleCenterWithDiseaseAndType(psychoReport, center, outdir):
    "report state including center, disease, and sequence type, ignoring not live at SRA"
    rg = ReportGenerator(psychoReport, ("study", "disease", "sequenceType", "center", "metadataState", "dataState"),
                         lambda row: (getBaseStudy(row.study), getBaseDisease(row.study,row.tumorCode), getBaseSeqType(row.exprAcc,row.center,row.sampleBarcode), row.center, getMetadataState(row), getDataState(row), row.filetype), #getFileType(row)), #getBaseAvgSize(row.center,getBaseSeqType(row.sampleBarcode))),
                         lambda row: (row.center==center) and inclInStateReport(row))
    rg.writeReport(outdir, center+"-state-disease-seqtype", True)

def reportStateSplitByCenter(psychoReport, outdir):
    for center in psychoReport.centers:
        reportStateSingleCenter(psychoReport, center, outdir)
        reportStateSingleCenterWithDiseaseAndType(psychoReport, center, outdir)

def reportState(psychoReport, outdir):
    "report state, ignoring not live at SRA"
    rg = ReportGenerator(psychoReport, ("study", "metadataState", "dataState"), 
                         lambda row: (getBaseStudy(row.study), getMetadataState(row), getDataState(row)),
                         inclInStateReport)
    rg.writeReport(outdir, "state")

def reportMetadataStateCenter(psychoReport, outdir):
    "report state including center, ignoring not live at SRA"
    rg = ReportGenerator(psychoReport, ("study", "center", "metadataState"),
                         lambda row: (getBaseStudy(row.study), row.center, getMetadataState(row)),
                         inclInStateReport)
    rg.writeReport(outdir, "center-metadata-state")

def reportDataStateCenter(psychoReport, outdir):
    "report state including center, ignoring not live at SRA"
    rg = ReportGenerator(psychoReport, ("study", "center", "dataState"),
                         lambda row: (getBaseStudy(row.study), row.center, getDataState(row)),
                         inclInStateReport)
    rg.writeReport(outdir, "center-data-state")

def reportMetadataState(psychoReport, outdir):
    "report metadata+state, ignoring not live at SRA"
    rg = ReportGenerator(psychoReport, ("study", "metadataState"), 
                         lambda row: (getBaseStudy(row.study), getMetadataState(row)),
                         inclInStateReport)
    rg.writeReport(outdir, "metadata-state")

def reportDataState(psychoReport, outdir):
    "report data+state, ignoring not live at SRA"
    rg = ReportGenerator(psychoReport, ("study", "dataState"), 
                         lambda row: (getBaseStudy(row.study), getDataState(row)),
                         inclInStateReport)
    rg.writeReport(outdir, "data-state")


def reportDeadWithData(psychoReport, outdir):
    "report on not-live at SRA where we have metadata or data "
    rg = ReportGenerator(psychoReport, ("study", "sraStatus", "center", "metadataState", "dataState"),
                         lambda row: (getBaseStudy(row.study), row.sraStatus, row.center, getMetadataState(row), getDataState(row)),
                         lambda row: "live" not in row.sraStatus)
    rg.writeReport(outdir, "not-sra-live-with-data")


with open(args.reportTsv) as reportTsvFh:
    psychoReport = PsychoReport(reportTsvFh, barcodeStudyMap)

reportState(psychoReport, args.outdir)
reportStateCenter(psychoReport, args.outdir)
reportMetadataState(psychoReport, args.outdir)
reportDataStateCenter(psychoReport, args.outdir)
reportDataState(psychoReport, args.outdir)
reportMetadataStateCenter(psychoReport, args.outdir)
reportStateSplitByCenter(psychoReport, args.outdir)
reportDeadWithData(psychoReport, args.outdir)
# FIXME: add
# problem case report: weird download, no runAcc srfBamSubmission
