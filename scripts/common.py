#!/usr/bin/env python

"""Common routines for metadata processing"""

import csv
import re

def removeExtraneousWhitespaceFromDict(rowDict):
    """Given a dictionary, return a new dictionary with the
    extraneous leading and trailing whitespace removed from all
    keys and values"""
    newRow = dict()
    for key in rowDict.keys():
        value = rowDict[key]
        newKey = re.sub("(\s)*$", "", re.sub("^(\s)*", "", key))
        newValue = re.sub("(\s)*$", "", re.sub("^(\s)*", "", value))
        newRow[newKey] = newValue
    return(newRow)


def cleanupDictOfDicts(dd):
    """Clean up a dict of dicts, by removing extraneous whitespace from
    the keys"""
    newDict = dict()
    for thisKey in dd.keys():
        newKey = re.sub("(\s)*$", "", re.sub("^(\s)*", "", thisKey))
        newDictDict = dict()
        for thisSubKey in dd[thisKey]:
            thisValue = dd[thisKey][thisSubKey]
            thisSubKey = re.sub("(\s)*$", "", re.sub("^(\s)*", "", thisSubKey))
            thisValue = re.sub("(\s)*$", "", re.sub("^(\s)*", "", thisValue))
            newDictDict[thisSubKey] = thisValue
        newDict[newKey] = newDictDict
    return(newDict)


def readDictOfDicts(filename, keyColumn=0, delimiter="\t", listAll=False):
    """Given a delimited file, return a dict of dicts, where the
    key to the outer dict is the contents of the first column and
    the keys to the inner dicts are the header row columns.  In other
    words, return a dict for which the key is the contents of the first
    column, and the value for each of those keys is a dict for which the
    keys are the column labels"""
    headerLine = open(filename).readline().rstrip()
    nameOfKeyColumn = re.split(delimiter, headerLine)[keyColumn]
    dictOfDicts = dict()
    dd = csv.DictReader(open(filename), delimiter=delimiter)
    for row in dd:
        key = row[nameOfKeyColumn]
        newKey = re.sub("(\s)*$", "", re.sub("^(\s)*", "", key))
        newDictDict = dict()
        for thisSubKey in row.keys():
            thisValue = row[thisSubKey]
            thisSubKey = re.sub("(\s)*$", "", re.sub("^(\s)*", "", thisSubKey))
            if thisValue is not None:
                thisValue = re.sub("(\s)*$", "", re.sub("^(\s)*", "",
                                                        thisValue))
            newDictDict[thisSubKey] = thisValue
        if not listAll:
            dictOfDicts[newKey] = newDictDict
        elif dictOfDicts.has_key(newKey):
            dictOfDicts[newKey].append(newDictDict)
        else:
            dictOfDicts[newKey] = list()
            dictOfDicts[newKey].append(newDictDict)
    return dictOfDicts

def ensureStudyRefIsComplete(root, studyAccessionMapping,
                             refcenter="National Cancer Institute"):
    """Check that the STUDY_REF object contains refname and refcenter
    attributes.  If they are missing, add them"""
    for studyRefObj in root.iter("STUDY_REF"):
        if studyRefObj.get("refcenter") is None:
            studyRefObj.set("refcenter", refcenter)
        elif studyRefObj.get("refcenter") == "dbgap":
            studyRefObj.set("refcenter", refcenter)
        if studyRefObj.get("refname") is None:
            projectAccession = studyRefObj.get("accession")
            if studyAccessionMapping.has_key(projectAccession):
                refname = studyAccessionMapping[projectAccession]["dbGaP_Accession"]
                studyRefObj.set("refname", refname) 
        

def ensureTargetIsComplete(root, refcenter="NCI_TARGET"):
    """Check that the TARGET object contains the refcenter, add if missing"""
    for studyRefObj in root.iter("STUDY_REF"):
        if studyRefObj.get("refcenter") is None:
            studyRefObj.set("refcenter", refcenter)
