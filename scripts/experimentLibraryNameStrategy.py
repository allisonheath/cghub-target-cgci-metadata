#!/usr/bin/env python

import argparse
import csv
import os
import xml.etree.ElementTree as ET



parser = argparse.ArgumentParser()
parser.add_argument("metadataSummaryFile", type=str,
                    help="The metadata summary file")
parsre.add_argument("metadataDir", type=str, 
                    help="Directory of per-submission metadata")
args = parser.parse_args()

metadataSummary = csv.DictReader(open(args.metadataSummaryFile), 
                                 delimiter="\t")
for row in metadataSummary:
    experimentFile = args.metadataDir + row.get("Submission") + "/experiment.xml"
    if not os.path.exists(experimentFile):
        print row.get("RunAccession"), row.get("Filename"), row.get("md5"), "Missing", "Missing"
    else:
        tree = ET.parse(experimentFile)
        root = tree.getroot()
        for object in root.iter("LIBRARY_DESCRIPTOR"):
            libraryName = object.find("LIBRARY_NAME").text
            libraryStrategy = object.find("LIBRARY_STRATEGY").text
        print row.get("RunAccession"), row.get("Filename"), row.get("md5"), libraryName, libraryStrategy
