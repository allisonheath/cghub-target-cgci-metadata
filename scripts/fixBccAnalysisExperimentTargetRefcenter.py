#!/usr/bin/env python

import argparse
import common
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET


def readXmlFile(pathname):
    """Read a given XML file, and return the root of the element tree"""
    assert(os.path.exists(pathname))
    tree = ET.parse(pathname)
    return(tree)


 
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("perObjectDir", type=str, 
                        help="Directory with each type of metadata object") 
    args = parser.parse_args()

    objectSetNames = ("analysis", "experiment")
    for thisSet in objectSetNames:
        for thisFile in os.listdir(args.perObjectDir + "/" + thisSet):
            thisPath = args.perObjectDir + "/" + thisSet + "/" + thisFile
            thisTree = readXmlFile(thisPath)
            thisXml = thisTree.getroot()
            for targetObj in thisXml.iter("TARGET"):
                refcenter = targetObj.get("refcenter")
                if refcenter == "NCI":
                    targetObj.set("refcenter", "NCI_TARGET")
                    thisTree.write(thisPath)
            for sdObj in thisXml.iter("SAMPLE_DESCRIPTOR"):
                refcenter = sdObj.get("refcenter")
                if refcenter == "NCI":
                    sdObj.set("refcenter", "NCI_TARGET")
                    thisTree.write(thisPath)
        
if __name__ == '__main__':
    main()

    

