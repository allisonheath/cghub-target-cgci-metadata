#!/usr/bin/env python

import argparse
import common
import os
import shutil
import subprocess
import sys
import uuid
import xml.etree.ElementTree as ET

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("lookupTable", type=str, 
                        help="Tab-delimited cross-reference file")
    parser.add_argument("--sourceDir", type=str, default="fromDivya", 
                        help="Input Directory")
    parser.add_argument("--outputDir", type=str, 
                        default="repackaged/TARGET/BCM",
                        help="Output directory for repackaged submissions")
    args = parser.parse_args()
    lookupData = common.readDictOfDicts(args.lookupTable, keyColumn=29,
                                        delimiter='\t')
    studies = ('target-all', 'target-aml', 'target-wilms', 
               'target-xenograft')
    for thisStudy in studies:
        studyDir = args.sourceDir + "/" + thisStudy
        for subdir in os.listdir(studyDir):
            submissionsDir = studyDir + "/" + subdir
            for submission in os.listdir(submissionsDir):
                saveSubmission(submissionsDir, submission, lookupData, 
                               args.outputDir)
                

def saveSubmission(studyDir, submission, lookupData, outputDir):
    print "copying submission", submission, "from", studyDir
    assert(lookupData.has_key(submission))
    inputDirThisSubmission = studyDir + "/" + submission
    runAccession = lookupData[submission]["Run"]
    myUuid = uuid.uuid5(uuid.NAMESPACE_URL, 
                        runAccession).urn.split(":")[-1]
    outputDirThisSubmission = outputDir + "/" + myUuid
    if not os.path.exists(outputDirThisSubmission):
        os.mkdir(outputDirThisSubmission)
    analysisFile = inputDirThisSubmission + "/new.analysis.xml"
    assert(os.path.exists(analysisFile))
    shutil.copy(analysisFile, outputDirThisSubmission + "/analysis.xml")
    runFile = inputDirThisSubmission + "/" + submission + ".run.xml"
    assert(os.path.exists(runFile))
    shutil.copy(runFile, outputDirThisSubmission + "/run.xml")
    experimentFile = inputDirThisSubmission + "/" + submission + ".experiment.xml"
    assert(os.path.exists(experimentFile))
    shutil.copy(experimentFile, outputDirThisSubmission + "/experiment.xml")
    
    

           

if __name__ == '__main__':
    main( )



