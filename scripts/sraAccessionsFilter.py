#!/usr/bin/env python

import argparse, re, os, sys, csv, glob
from studies import studies

# Since some suppressed runs don't have study or experiment, just `-' (e.g.
# SRR616392), we make two passed.  One to collect all know TARGET/CGCI
# accessions and another one to output them, including lines where the
# ReplacedBy is a TARGET/CGCI.

def cmdParse():
    "parse command line and return arguments object"
    parser = argparse.ArgumentParser(description="""filter SRA_Accessions for only TARGET/CGCI studies.""")
    parser.add_argument("fullSraAccessions", type=str,
                        help="""full SRA_Accessions file""")
    parser.add_argument("filteredSraAccessions", type=str,
                        help="""filtered SRA_Accessions""")
    return parser.parse_args()

class excel_tab_noquoting(csv.excel_tab):
    """SRA_Accessions has columns like `"Institute of Vegetables and Flowers,
    Chinese Acad', so this just disables quote processing.  """
    quoting = csv.QUOTE_NONE

def addAccIfSpecified(tsvRow, column, targetCgciAccSet):
    acc = tsvRow[column]
    if acc not in ("", "-"):
        targetCgciAccSet.add(acc)

def addTargetCgciAccs(tsvRow, targetCgciAccSet):
    for column in ("Accession", "Experiment", "ReplacedBy"):
        addAccIfSpecified(tsvRow, column, targetCgciAccSet)

def buildSraAccSet(inAccessionsTsvFh):
    targetCgciAccSet = set()
    for tsvRow in csv.DictReader(inAccessionsTsvFh, dialect=excel_tab_noquoting):
        if studies.getByAnyAcc(tsvRow["Study"]) != None:
            addTargetCgciAccs(tsvRow, targetCgciAccSet)
    return targetCgciAccSet

def isTargetCgciRow(targetCgciAccSet, tsvRow):
    return (tsvRow["Accession"] in targetCgciAccSet) or (tsvRow["ReplacedBy"] in targetCgciAccSet) or (studies.getByAnyAcc(tsvRow["Study"]) != None)

def filterSraAccessions(targetCgciAccSet, inAccessionsTsvFh, outAccessionsTsv):
    inAccessionsTsv = csv.DictReader(inAccessionsTsvFh, dialect=excel_tab_noquoting)
    outAccessionsTsv = csv.DictWriter(outAccessionsTsvFh, inAccessionsTsv.fieldnames, dialect=excel_tab_noquoting)
    outAccessionsTsv.writeheader()
    for tsvRow in inAccessionsTsv:
        if isTargetCgciRow(targetCgciAccSet, tsvRow):
            outAccessionsTsv.writerow(tsvRow)

args = cmdParse()
with open(args.fullSraAccessions, "U") as inAccessionsTsvFh:
    targetCgciAccSet = buildSraAccSet(inAccessionsTsvFh)
with open(args.fullSraAccessions, "U") as inAccessionsTsvFh:
    with open(args.filteredSraAccessions, "w") as outAccessionsTsvFh:
     filterSraAccessions(targetCgciAccSet, inAccessionsTsvFh, outAccessionsTsvFh)
