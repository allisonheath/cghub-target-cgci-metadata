#!/usr/bin/env python

import argparse
import common
import csv
import os
import re
import shutil
import subprocess
import sys

parser = argparse.ArgumentParser()
parser.add_argument('runTableFile', type=str, help="SRA Run Table")
parser.add_argument('accessionsFile', type=str, help="SRA Accessions file")
parser.add_argument('metadataTarFile', type=str,
                    help="SRA Metadata dump")
parser.add_argument('--byCenter', type=str, default=True, 
                    help="Indicates whether to split the metadata by center")
args = parser.parse_args()

csv.field_size_limit(sys.maxsize)
accessions = common.readDictOfDicts(args.accessionsFile)
metadataDirname = re.sub(".tar.gz$", "", args.metadataTarFile)
runTable = csv.DictReader(open(args.runTableFile), delimiter="\t")
submissionsUnpacked = dict()
for row in runTable:
    cleanedRow = common.removeExtraneousWhitespaceFromDict(row)
    runAccession = cleanedRow['Run']
    centerName = cleanedRow['Center Name']
    if accessions[runAccession]['Type'] != 'RUN':
        raise ValueError("Unexpected accession type %s from accession %s"
                         % (accessions[runAccession]['Type'], runAccession))
    if accessions.has_key(runAccession):
        submissionAccession = accessions[runAccession]['Submission']
        if not submissionsUnpacked.has_key(submissionAccession):
            print "Getting %s from run accession %s" % (submissionAccession,
                                                        runAccession)
            submissionDir = "%s/%s" % (metadataDirname, submissionAccession)
            untarCmd = "tar xzvf %s %s" % (args.metadataTarFile,
                                           submissionDir)
            #
            # It turns out to be a nightmare to keep the tarball, accessions
            # file and SRA run table all synchronized.  Given that, accept that
            # we might have some accessions that aren't in the taball, and 
            # deal with it by catching exceptions on the untar command.
            #
            try:
                subprocess.check_output(untarCmd, shell=True)
            except:
                pass
            else:
                if args.byCenter:
                    if os.path.exists(metadataDirname):
                        centerSubdirectory = "%s/%s" % (metadataDirname, 
                                                        centerName)
                        if not os.path.exists(centerSubdirectory):
                            os.mkdir(centerSubdirectory)
                        shutil.move(submissionDir, centerSubdirectory)
                submissionsUnpacked[submissionAccession] = 1
